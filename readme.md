# Daniels dotfiles

Easily setup a new mac.

```sh
cd 
git clone git@bitbucket.org:lendai/dotfiles.git
cd dotfiles
./install.sh

open ~/dotfiles/.gitconfig
```

This will;
* Setup the terminal
* Set a bunch of default values for macos
* Install the applications i use often
